import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap,Router} from '@angular/router'
import {HttpClient} from '@angular/common/http'

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {

  constructor(private route:ActivatedRoute,private http:HttpClient,private router:Router) { }
  boardId:any
  boardName:any
  lists:any=[]
  listId:any=[]

  ngOnInit(): void {
    this.route.paramMap.subscribe((params:ParamMap)=>{
       let id:any=params.get('id')
       let name:any=params.get('name')
       this.boardId=id
       this.boardName=name
       this.http.get<any>(`https://api.trello.com/1/boards/${this.boardId}/lists?key=e4b333731978f610da2a14d6f815fa6c&token=79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef`).subscribe((data)=>{
       console.log(data)
          data.map((i:any,k:any)=>{
          this.http.get<any>(`https://api.trello.com/1/lists/${i['id']}/cards?key=e4b333731978f610da2a14d6f815fa6c&token=79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef`).subscribe((cards)=>{
            let cardArr:any=[]
            let cardId:any=[]
            for(let j of cards){
              cardArr.push(j['name'])
              cardId.push(j['id'])
            }
            this.lists[k]=({name:i['name'],cards:cardArr})
            this.listId[k]=({id:i['id'],cardId:cardId})
          })
        })
    })
    })
  }

  deleteCard(listIndex:any,cardIndex:any) {
    this.lists[listIndex]['cards'].splice(cardIndex,1)
    let id=this.listId[listIndex]['cardId'][cardIndex]
    fetch(`https://api.trello.com/1/cards/${id}?key=e4b333731978f610da2a14d6f815fa6c&token=79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef`, {
    method: 'DELETE'
    })
    .then(response => {
      console.log(
        `Response: ${response.status} ${response.statusText}`
      );
      return response.text();
    })
    .then(text => console.log(text))
    .catch(err => console.error(err));
  }

  addCard(event:any,input:any,listIndex:any) {
    event.preventDefault()
     let name=input.value
     let id=this.listId[listIndex]['id']
     let length=this.lists[listIndex]['cards'].length
     this.lists[listIndex]['cards'][parseInt(length)]=name
     fetch(`https://api.trello.com/1/cards?key=e4b333731978f610da2a14d6f815fa6c&token=79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef&idList=${id}&name=${name}`, {
     method: 'POST'
     })
    .then(response => {
      console.log(
        `Response: ${response.status} ${response.statusText}`
      );
      return response.json();
    })
    .then(data => {
      this.listId[listIndex]['cardId'][parseInt(length)]=data['id']
    })
    .catch(err => console.error(err));

    input.value=""
  }

  createList(input:any) {
    let name=input.value
    fetch(`https://api.trello.com/1/lists?key=e4b333731978f610da2a14d6f815fa6c&token=79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef&name=${name}&idBoard=${this.boardId}`, {
    method: 'POST'
    })
    .then(response => {
      console.log(
        `Response: ${response.status} ${response.statusText}`
      );
      return response.text();
    })
    .then(text => console.log(text))
    .catch(err => console.error(err));
    }

  getCard(i:any,j:any) {
    let id=this.listId[i]['cardId'][j]
    let listName=this.lists[i]['name']
    this.router.navigate(['card',{cardId:id,listName:listName}],{relativeTo:this.route})
  }
}