import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap,Router} from '@angular/router'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

  boardId:any

  constructor(private route:ActivatedRoute,private router:Router) { 

  }

  clickHandler() {
    this.router.navigate(['/boards'])
  }


  ngOnInit(): void {
    this.route.paramMap.subscribe((params:ParamMap)=>{
      let id:any=params.get('id')
      let name:any=params.get('name')
      this.boardId=id
      console.log(this.boardId)
  })
  }
}