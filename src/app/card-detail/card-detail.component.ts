import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router, ParamMap} from '@angular/router'

@Component({
  selector: 'app-card-detail',
  templateUrl: './card-detail.component.html',
  styleUrls: ['./card-detail.component.css']
})
export class CardDetailComponent implements OnInit {

  constructor(private route:ActivatedRoute,private router:Router) { }
  cardId:any
  card:any={'description':'','checklists':[],'checkItems':[],'checklistId':[]}
  check:any=false
  checklist:any=[]
  key:any='e4b333731978f610da2a14d6f815fa6c'
  token:any='79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef'
  ngOnInit(): void {

    this.route.paramMap.subscribe((params:ParamMap)=>{
      this.cardId=params.get("cardId")
      this.card.listName=params.get("listName")
    })
    let id=this.cardId
    console.log(id)
    fetch(`https://api.trello.com/1/cards/${id}?key=e4b333731978f610da2a14d6f815fa6c&token=79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef`, {
    method: 'GET',
    headers: {
      'Accept': 'application/json'
    }
    })
    .then(response => {
      console.log(
        `Response: ${response.status} ${response.statusText}`
      );
      return response.json();
    })
    .then(data => {
      console.log(data)
      this.card['cardName']=data['name']
      this.card['boardId']=data['idBoard']
      this.card['listId']=data['idList']
      this.card['description']=data['desc']
     // this.card['checklistId']=data['idChecklists']
    }) 
    .catch(err => console.error(err));
    this.getCheckLists()
  }


  close() {
    this.router.navigate(['../'],{relativeTo:this.route})
  }

  closeChecklist() {
     this.check=!this.check
  }

  addDescription(input:any) {
    fetch(`https://api.trello.com/1/cards/${this.cardId}?key=e4b333731978f610da2a14d6f815fa6c&token=79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef&desc=${input.value}`, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json'
      }
    })
      .then(response => {
        console.log(
          `Response: ${response.status} ${response.statusText}`
        );
        return response.text();
      })
      .then(text => console.log(text))
      .catch(err => console.error(err));
  }

  highlight(input:any) {
    input.style.border="1px solid black"
  }

  getCheckLists() {
    fetch(`https://api.trello.com/1/cards/${this.cardId}/checklists?key=${this.key}&token=${this.token}`, {
    method: 'GET'
    })
    .then(response => {
      console.log(
        `Response: ${response.status} ${response.statusText}`
      );
      console.log('chekclists')
      return response.json();
    })
    .then(data => {
      console.log(data)
       data.map((checklist:any,index:any)=>{
           this.getCheckItems(checklist['id'])
           .then((checkItemArr:any)=>{
                this.card.checklists.push({name:checklist['name'],checkItems:checkItemArr['names']})
                this.card.checklistId.push({id:checklist['id'],checkItemsIds:checkItemArr['ids']})
           })
       })
    })
    .catch(err => console.error(err));
  }

  getCheckItems(id:any) {
    return new Promise((resolve,reject)=>{
      fetch(`https://api.trello.com/1/checklists/${id}/checkItems?key=e4b333731978f610da2a14d6f815fa6c&token=79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef`, {
      method: 'GET'
      })
      .then(response => {
        console.log(
          `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
      })
      .then(data =>{ 
        let checkItems:any=[]
        let checkItemIds:any=[]
        data.map((item:any,index:any)=>{
          checkItems.push(item['name'])
          checkItemIds.push(item['id'])
        })
        resolve({'names':checkItems,'ids':checkItemIds})
      })
      .catch(err => reject(err));
    })
  }

  deleteChecklist(checkListIndex:any) {
      console.log(checkListIndex)
      let id=this.card.checklistId[checkListIndex]['id']
      console.log(id)
      this.apiDeleteChecklist(id,checkListIndex)
      this.card.checklistId.splice(checkListIndex,1)
  }
  
  deleteCheckItem(checkListIndex:any,checkItemIndex:any) {
      let checkListId=this.card.checklistId[checkListIndex]['id']
      console.log("checklistId",checkListId)
      let checkItemId=this.card.checklistId[checkListIndex]['checkItemsIds'][checkItemIndex]
      console.log('itemid',checkItemId)
      this.card.checklistId[checkItemIndex]['checkItemsIds'].splice(checkItemIndex,1)
      fetch(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${this.key}&token=${this.token}`, {
      method: 'DELETE'
      })
      .then(response => {
        console.log(
          `Response: ${response.status} ${response.statusText}`
        );
        return response.text();
      })
      .then(text => this.card.checklists[checkListIndex]['checkItems'].splice(checkItemIndex,1))
      .catch(err => console.error(err));
  }
   
  apiDeleteChecklist(id:any,i:any) {
    fetch(`https://api.trello.com/1/checklists/${id}?key=${this.key}&token=${this.token}`, {
      method: 'DELETE'
      })
      .then(response => {
        console.log(
          `Response: ${response.status} ${response.statusText}`
        );
        return response.text();
      })
      .then(text => this.card.checklists.splice(i,1))
      .catch(err => console.error(err));
  }

  addCheckList(event:any,input:any) {
    event.preventDefault()
    let name=input.value
    let length=this.card.checklists.length
    this.card.checklists[length]={'name':name,'checkItems':[]}

    fetch(`https://api.trello.com/1/checklists?key=${this.key}&token=${this.token}&idCard=${this.cardId}&name=${name}`, {
    method: 'POST'
    })
    .then(response => {
      console.log(
        `Response: ${response.status} ${response.statusText}`
      );
      return response.json();
    })
    .then(data => this.card.checklistId.push({'id':data['id'],'checkItemsIds':[]}))
    .catch(err => console.error(err));
    input.value=""
  }

  addCheckItem(input:any,index:any) {
     let name=input.value
     let length=this.card.checklists[index]['checkItems'].length
     this.card.checklists[index]['checkItems'][length]=name
     let checklistId=this.card.checklistId[index]['id']
     console.log(checklistId)
     fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${this.key}&token=${this.token}&name=${name}`, {
      method: 'POST'
     })
      .then(response => {
        console.log(
          `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
      })
      .then(data =>{
        this.card.checklistId[index]['checkItemsIds'].push(data['id'])
        console.log(data['id'],'oy')
      })
      .catch(err => console.error(err));
      input.value=""
  }
}