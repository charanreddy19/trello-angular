import { NgModule } from '@angular/core';
import { Routes, RouterModule,RouterLink } from '@angular/router';
import { BoardsComponent } from './boards/boards.component';
import {BoardComponent} from './board/board.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CardDetailComponent } from './card-detail/card-detail.component';

const routes: Routes = [
  {path:'',redirectTo:'/boards',pathMatch:'full'},
  {path:'boards',component:BoardsComponent},
  {
    path:'board',
    component:BoardComponent,
    children:[
      {path:'card',component:CardDetailComponent}
    ]
  },
  {path:'**',component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
