import { Component, OnInit } from '@angular/core';
import {HttpClient,HttpErrorResponse} from '@angular/common/http'
import {Router,RouterLink} from '@angular/router'

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css']
})
export class BoardsComponent implements OnInit {
  url:any="https://api.trello.com/1/members/me/boards?fields=name,url&key=e4b333731978f610da2a14d6f815fa6c&token=79c5fb0f5f02cdd4f810781a8485eea03299fcef8baea2979552080999a0c7ef"
  boards:any=[]
  
  constructor(private http:HttpClient,private router:Router) { }

  ngOnInit(): void {
     this.http.get<any>(this.url).subscribe((data)=>{
          for(let i of data) {
            this.boards.push({name:i['name'],id:i['id']})
            console.log(this.boards)
          }
      })
  }

  navigate(board:any) {
    this.router.navigate(['/board',{id:board.id,name:board.name}])
  }

}